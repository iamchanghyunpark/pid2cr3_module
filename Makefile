# Reference: https://linux-kernel-labs.github.io/master/labs/kernel_modules.html#exercises
KDIR=/lib/modules/`uname -r`/build

kbuild:
	make -C $(KDIR) M=`pwd`

clean:
	make -C $(KDIR) M=`pwd` clean
