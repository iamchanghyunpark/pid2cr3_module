# PID2CR3_module

Kernel module that prints out the physical address for the CR3 register for a
given PID. This module creates a sysfs file at `/sys/kernel/pid2cr3/pid2cr3`.
Write a PID into the sysfs file, and then read from the same file. The hex
value for the PA of the CR3 should be printed out. (With 0x prefix).

Usage example:

```
# echo 28458 > /sys/kernel/pid2cr3/pid2cr3
# cat /sys/kernel/pid2cr3/pid2cr3
0x131341000
#
```

Tested on:
 * 5.4.0
