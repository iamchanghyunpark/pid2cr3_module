#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kobject.h>
#include <linux/sysfs.h>
#include <linux/string.h>
#include <linux/pid.h>
#include <linux/mm.h>
#include <linux/io.h>

/**
    Created by referenceing 
     * kernel module tutorial at https://linux-kernel-labs.github.io/master/labs/kernel_modules.html#exercises
     * sysfs tutorial at http://pradheepshrinivasan.github.io/2015/07/02/Creating-an-simple-sysfs/
     * pid2cr3 code from https://carteryagemann.com/pid-to-cr3.html (The code is an exact copy from the site)

*/

MODULE_DESCRIPTION("Kernel module to translate given PID to CR3 physical address");
MODULE_AUTHOR("Chang Hyun Park");
MODULE_LICENSE("GPL");

//Globals
int pid_to_lookup;
unsigned long cr3_to_return;
struct kobject *kobj_ref;

unsigned long pid_to_cr3(int pid)
{
    struct task_struct *task;
    struct mm_struct *mm;
    void *cr3_virt;
    unsigned long cr3_phys;

    task = pid_task(find_vpid(pid), PIDTYPE_PID);

    if (task == NULL)
        return 0; // pid has no task_struct

    mm = task->mm;

    // mm can be NULL in some rare cases (e.g. kthreads)
    // when this happens, we should check active_mm
    if (mm == NULL) {
        mm = task->active_mm;
    }

    if (mm == NULL)
        return 0; // this shouldn't happen, but just in case

    cr3_virt = (void *) mm->pgd;
    if (!virt_addr_valid(cr3_virt)) {
        printk(KERN_INFO "cr3_virt 0x%lx is not a valid virtual address\n", (unsigned long)cr3_virt);
        return 0;
    }
    cr3_phys = virt_to_phys(cr3_virt);

    return cr3_phys;
}

static ssize_t read_cr3(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    printk(KERN_INFO "pid %d to cr3: 0x%lx\n", pid_to_lookup, cr3_to_return);
    return snprintf(buf, 64, "0x%lx\n", cr3_to_return);
}

static ssize_t write_pid(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d", &pid_to_lookup);
    printk(KERN_INFO "PID received from user is %d\n", pid_to_lookup);

    if (pid_to_lookup > 0)
        cr3_to_return = pid_to_cr3(pid_to_lookup);
    return count;
}

static struct kobj_attribute pid2cr3_kobj_attr = __ATTR(pid2cr3, 0644, read_cr3, write_pid);

int prepare_sysfs_files(void)
{
    int ret;
    // Path: sys/kernel/pid2cr3
    kobj_ref = kobject_create_and_add("pid2cr3", kernel_kobj);

    if (kobj_ref == NULL) {
        printk(KERN_INFO "Error creating sysfs directory\n");
        ret = -1;
        goto kobj_release;
    }

    ret = sysfs_create_file(kobj_ref, &pid2cr3_kobj_attr.attr);
    if (ret) {
        printk(KERN_INFO "Error creating sysfs file\n");
        goto sysfs_file_remove;
    }

    return 0;

sysfs_file_remove:
    sysfs_remove_file(kobj_ref, &pid2cr3_kobj_attr.attr);
    // actually unnecessary, failure at sysfs_create_file should call this,
    // but just for sake of completeness of code

kobj_release:
    kobject_put(kobj_ref);
    kobj_ref = NULL;

    return ret;
}

void teardown_sysfs_files(void)
{
    kobject_put(kobj_ref);
}


static int pid2cr3_init(void)
{
	pid_to_lookup = -1;
	cr3_to_return = 0UL;
    kobj_ref = NULL;

    prepare_sysfs_files();

    printk("%s\n", __func__);
    return 0;
}

static void pid2cr3_exit(void)
{
    teardown_sysfs_files();
    printk("%s\n", __func__);
}

module_init(pid2cr3_init);
module_exit(pid2cr3_exit);
